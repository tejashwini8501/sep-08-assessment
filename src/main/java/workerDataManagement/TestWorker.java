package workerDataManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestWorker {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Worker worker = new Worker();
        WorkerData wd = new WorkerData();
        WorkerQuery wq = new WorkerQuery();

        System.out.println(wq.getFullName());
        System.out.println(wq.getDepartment());
        wq.getPosition(worker);

        while (true)
        {
            System.out.println("Enter your choice 1.Add a worker 2.update worker details 3.delete a worker 4.display all workers 5.exit");
            int choice = sc.nextInt();
            switch (choice)
            {
                case 1:
                {
                    System.out.println("enter worker id: ");
                    int worker_id = sc.nextInt();


                    System.out.println("enter first name: ");
                    String first_name = sc.next();

                    System.out.println("enter last name: ");
                    String last_name = sc.next();

                    System.out.println("enter salary: ");
                    double salary = sc.nextDouble();

                    System.out.println("enter joining date ");
                    String  joining_date = sc.nextLine();
                    sc.nextLine();

                    System.out.println("enter department ");
                    String department = sc.next();

                    worker.setWorker_id(worker_id);
                    worker.setFirst_name(first_name);
                    worker.setLast_name(last_name);
                    worker.setSalary(salary);
                    worker.setJoining_date(joining_date);
                    worker.setDepartment(department);

                    int x = wd.saveWorker(worker);
                    if(x>0)
                    {
                        System.out.println("One worker inserted");
                    }
                }break;

                case 2:
                {
                    System.out.println("enter worker id: ");
                    int worker_id = sc.nextInt();

                    System.out.println("enter updated salary: ");
                    double salary = sc.nextDouble();

                    System.out.println("enter updated department: ");
                    String department = sc.next();

                    worker.setWorker_id(worker_id);
                    worker.setSalary(salary);
                    worker.setDepartment(department);

                    int x = wd.updateWorker(worker);
                    if(x>0)
                    {
                        System.out.println("one worker updated");
                    }
                }break;

                case 3:
                {
                    System.out.println("enter worker id: ");
                    int worker_id = sc.nextInt();

                    worker.setWorker_id(worker_id);

                    int x = wd.deleteWorker(worker);
                    if (x>0)
                    {
                        System.out.println("one worker deleted");
                    }
                }break;

                case 4:
                {
                    List<Worker> workerList = new ArrayList<Worker>();
                    workerList = wd.getAllWorkers();

                    for(Worker w : workerList)
                    {
                        System.out.println("Worker ID : "+w.getWorker_id()+" First Name : "+w.getFirst_name()+" Last name : "+w.getLast_name()+"" +
                                " Salary : " +w.getSalary()+" Joining date : "+w.getJoining_date()+" Department : "+w.getDepartment());
                    }
                }break;

                case 5:
                {
                    System.out.println("exiting the program");
                    System.exit(0);
                }

                default:
                    System.out.println("wrong input");
            }
        }

    }
}

package workerDataManagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class WorkerData {

    public Statement getStatement()
    {
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/WorkerDM?autoReconnect=true&useSSL=false", "root", "password");
            stmt = con.createStatement();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return stmt;
    }

    public int saveWorker(Worker worker)
    {
        String sql = null;
        try {
            sql = "insert into worker values(" + worker.getWorker_id() + ", '" + worker.getFirst_name() + "'," +
                    "'" + worker.getLast_name() + "'," + worker.getSalary() + ", '"+ worker.getJoining_date() +
                    "','" + worker.getDepartment() + "')";

            return getStatement().executeUpdate(sql);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    public int updateWorker(Worker worker)
    {
        try {
            String sql = "update worker set salary ="+ worker.getSalary() +", department = '"+ worker.getDepartment() +"' " +
                    " where worker_id = "+ worker.getWorker_id() +"";

            return getStatement().executeUpdate(sql);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    public int deleteWorker(Worker worker)
    {
        try{
            String sql = "delete from worker where worker_id = "+ worker.getWorker_id() +" ";
            return getStatement().executeUpdate(sql);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    public List<Worker> getAllWorkers()
    {
        List<Worker> workerList = new ArrayList<Worker>();

        try {
            String sql = "select * from worker";
            ResultSet rs = getStatement().executeQuery(sql);
            while (rs.next())
            {
                Worker worker = new Worker();

                worker.setWorker_id(rs.getInt(1));
                worker.setFirst_name(rs.getString(2));
                worker.setLast_name(rs.getString(3));
                worker.setSalary(rs.getDouble(4));
                worker.setJoining_date(rs.getString(5));
                worker.setDepartment(rs.getString(6));

                workerList.add(worker);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return workerList;
    }

}

package workerDataManagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class WorkerQuery {

    public Statement getStatement() {
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/WorkerDM?autoReconnect=true&useSSL=false", "root", "password");
            stmt = con.createStatement();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return stmt;
    }

    public List<String> getFullName()
    {
        List<String> fullNameList = new ArrayList<>();

        try {
            String sql = "select concat(first_name, ' ', last_name) as full_name from worker;";
            ResultSet rs = getStatement().executeQuery(sql);
            while (rs.next())
            {
               fullNameList.add(rs.getString(1));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return fullNameList;
    }

    public List<String > getDepartment()
    {
        List<String > deptList = new ArrayList<>();

        try {
            String sql = "select distinct department from worker;";
            ResultSet rs = getStatement().executeQuery(sql);
            while (rs.next())
            {
                deptList.add(rs.getString(1));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return deptList;
    }

    public void getPosition(Worker worker)
    {
        try {
            String sql = "select position('a' in first_name) from worker where first_name = 'Amitabh';";
            ResultSet rs = getStatement().executeQuery(sql);
            if (rs.next()) {
                System.out.println(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
